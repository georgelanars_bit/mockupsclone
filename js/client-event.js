$(function(){
    
    $('.enable_toggler').on('click', function() {
        $(this).prop('readonly', false);
    });
      
    $('.enable_toggler').on('focusout', function() {
        $(this).prop('readonly', true);
    });
    
    var currentSelect;
    
    
    $('select.enable_toggler').on("focusin", function () {
        $(currentSelect).addClass('not-active_select');
        currentSelect = this;
        
        if ($(this).hasClass('not-active_select')) {
            $(this).removeClass('not-active_select');
            $('body').on("click", addNoActiveClass)
        } else {
            $(this).addClass('not-active_select');
            $(this).blur();
        }
    });
    
    function addNoActiveClass () {
        $(currentSelect).addClass('not-active_select');
        $('body').unbind( "click" );
        $(currentSelect).blur();
    }   
    
    $('.enable_toggler').keyup(function(e){
        var code = e.which;
        if (code==13) {
            $(this).prop('readonly', true);
        } else {}
    }); 
    
});